#include<iostream>
#include<conio.h>
#include<dos.h>
#include<windows.h>
#include<time.h>
#include<Game.h>

#define SCREEN_WIDTH 100
#define SCREEN_HEIGHT 30
#define WIN_WIDTH 70

using namespace std;


int main()
{
    Game g;
    g.setcursor(0,0);

	srand( (unsigned)time(NULL));

	do
    {
		system("cls");
		g.position(10,5); cout<<" -------------------------- ";
		g.position(10,6); cout<<" |        Car Game        | ";
		g.position(10,7); cout<<" --------------------------";
		g.position(10,9); cout<<"1. Start Game";
		g.position(10,10); cout<<"2. Instructions";
		g.position(10,11); cout<<"3. Quit";
		g.position(10,13); cout<<"Select option: ";
		char op = getche();

		if( op=='1') g.play();
		else if( op=='2') g.instructions();
		else if( op=='3') exit(0);

	}while(1);


    return 0;
}
