#ifndef GAME_H
#define GAME_H

#include<iostream>
#include<conio.h>
#include<dos.h>
#include<windows.h>
#include<time.h>

#define SCREEN_WIDTH 100
#define SCREEN_HEIGHT 30
#define WIN_WIDTH 70

using namespace std;

HANDLE console=GetStdHandle(STD_OUTPUT_HANDLE);
COORD CursorPosition;

int enemy1[3];
int enemy2[3];
int enemy3[3];
char car[4][4] = { ' ','�','�',' ',
                    '�','�','�','�',
                    ' ','�','�',' ',
                    '�','�','�','�' };


int carPos = WIN_WIDTH/2;
int score = 0;

class Game
{
    public:
        Game()
        {
            int enemy1[3];
            int enemy2[3];
            int enemy3[3];
            char car[4][4] = { ' ','�','�',' ',
                                '�','�','�','�',
                                ' ','�','�',' ',
                                '�','�','�','�' };
        }
        virtual ~Game()
        {
            delete[] car;
        }
        void position(int x, int y)
        {
            CursorPosition.X = x;
            CursorPosition.Y = y;
            SetConsoleCursorPosition(console, CursorPosition);
        }
        void setcursor(bool visible, DWORD size)
        {
            if(size == 0)
                size = 20;

            CONSOLE_CURSOR_INFO lpCursor;
            lpCursor.bVisible = visible;
            lpCursor.dwSize = size;
            SetConsoleCursorInfo(console,&lpCursor);
        }
        void drawBorder()
        {
            SetConsoleTextAttribute(console, 7);
            for(int i=0; i<SCREEN_HEIGHT; i++)
            {
                for(int j=0; j<17; j++)
                {
                position(0+j,i); cout<<"�";
                position(WIN_WIDTH-j,i); cout<<"�";
                }
            }
            for(int i=0; i<SCREEN_HEIGHT; i++)
            {
                position(SCREEN_WIDTH,i); cout<<"�";
            }
            SetConsoleTextAttribute(console, 3);
        }

        void generateEnemy(int ind)
        {
            enemy1[ind] = 17 + rand()%(33);
        }
        void drawEnemy(int ind)
        {
            SetConsoleTextAttribute(console, 4);
            if( enemy3[ind] == true )
            {
                position(enemy1[ind], enemy2[ind]);   cout<<"****";
                position(enemy1[ind], enemy2[ind]+1); cout<<" ** ";
                position(enemy1[ind], enemy2[ind]+2); cout<<"****";
                position(enemy1[ind], enemy2[ind]+3); cout<<" ** ";
            }
            SetConsoleTextAttribute(console, 3);
        }
        void eraseEnemy(int ind)
        {
            if( enemy3[ind] == true )
            {
                position(enemy1[ind], enemy2[ind]); cout<<"    ";
                position(enemy1[ind], enemy2[ind]+1); cout<<"    ";
                position(enemy1[ind], enemy2[ind]+2); cout<<"    ";
                position(enemy1[ind], enemy2[ind]+3); cout<<"    ";
            }
        }
        void resetEnemy(int ind)
        {
            eraseEnemy(ind);
            enemy2[ind] = 1;
            generateEnemy(ind);
        }

        void drawCar()
        {
            for(int i=0; i<4; i++)
            {
                for(int j=0; j<4; j++)
                {
                    position(j+carPos, i+22); cout<<car[i][j];
                }
            }
        }
        void eraseCar()
        {
            for(int i=0; i<4; i++){
                for(int j=0; j<4; j++)
                {
                    position(j+carPos, i+22); cout<<" ";
                }
            }
        }

        int collision()
        {
            if( enemy2[0]+4 >= 23 )
            {
                if( enemy1[0] + 4 - carPos >= 0 && enemy1[0] + 4 - carPos < 9  )
                {
                    return 1;
                }
            }
            return 0;
        }
        void gameover()
        {
            system("cls");
            cout<<endl;
            cout<<"\t\t--------------------------"<<endl;
            cout<<"\t\t-------- Game Over -------"<<endl;
            cout<<"\t\t--------------------------"<<endl<<endl;
            cout<<"\t\tPress any key to go back to menu.";
            getch();
        }
        void updateScore()
        {
            SetConsoleTextAttribute(console, 2);
            position(WIN_WIDTH + 7, 5);cout<<"Score: "<<score<<endl;
            SetConsoleTextAttribute(console, 3);
        }

        void instructions()
        {

            system("cls");
            cout<<"Instructions";
            cout<<"\n----------------";
            cout<<"\n Avoid Cars by moving left or right. ";
            cout<<"\n\n Press 'a' to move left";
            cout<<"\n Press 'd' to move right";
            cout<<"\n Press 'escape' to exit";
            cout<<"\n\nPress any key to go back to menu";
            getch();
        }

        void play()
        {
            carPos = -1 + WIN_WIDTH/2;
            score = 0;
            enemy3[0] = 1;
            enemy3[1] = 0;
            enemy2[0] = enemy2[1] = 1;

            system("cls");
            drawBorder();
            updateScore();
            generateEnemy(0);
            generateEnemy(1);

            position(WIN_WIDTH + 7, 2);cout<<"Car Game";
            position(WIN_WIDTH + 6, 4);cout<<"----------";
            position(WIN_WIDTH + 6, 6);cout<<"----------";
            position(WIN_WIDTH + 7, 12);cout<<"Control ";
            position(WIN_WIDTH + 7, 13);cout<<"-------- ";
            position(WIN_WIDTH + 2, 14);cout<<" A Key - Left";
            position(WIN_WIDTH + 2, 15);cout<<" D Key - Right";

            position(18, 5);cout<<"Press any key to start";
            getch();
            position(18, 5);cout<<"                      ";

            while(1)
            {
                if(kbhit())
                {
                    char ch = getch();
                    if( ch=='a' || ch=='A' )
                    {
                        if( carPos > 18 )
                            carPos -= 4;
                    }
                    if( ch=='d' || ch=='D' )
                    {
                        if( carPos < 50 )
                            carPos += 4;
                    }
                    if(ch==27)
                    {
                        break;
                    }
                }

                drawCar();
                drawEnemy(0);
                drawEnemy(1);
                if( collision() == 1  )
                {
                    gameover();
                    return;
                }
                Sleep(50);
                eraseCar();
                eraseEnemy(0);
                eraseEnemy(1);

                if( enemy2[0] == 10 )
                    if( enemy3[1] == 0 )
                        enemy3[1] = 1;

                if( enemy3[0] == 1 )
                    enemy2[0] += 1;

                if( enemy3[1] == 1 )
                    enemy2[1] += 1;

                if( enemy2[0] > SCREEN_HEIGHT-4 )
                {
                    resetEnemy(0);
                    score++;
                    updateScore();
                }
                if( enemy2[1] > SCREEN_HEIGHT-4 )
                {
                    resetEnemy(1);
                    score++;
                    updateScore();
                }
            }
        }

    protected:

    private:
};

#endif // GAME_H


